var debug = process.env.NODE_ENV !== 'production';
var webpack = require('webpack');

module.exports = {
	context: __dirname+'/src',
	devtool: 'source-map',
	entry: "./app/app.js",
	module: {
		loaders: [{
			test: /\.js?$/,
			exclude: /(node_module|bower_components)/,
			loader: 'babel-loader',
			query: {
				presets: ['react', 'es2015', 'stage-0'],
				plugins: []
			}
		}]
	},
	output: {
		path: __dirname+'/src/app/',
		filename: "app.min.js"
	},
	plugins: debug? [] : [
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false})
	]
};