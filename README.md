# React Test

This is an example of a React app, to see all the interactions and details see [PDF].


### Version
1.0.0


## Installation

Dillinger requires [Node.js](https://nodejs.org/) v4+ to run.

```
$ git clone https://bitbucket.org/joelcorrales/react-test.git react-test
$ cd react-test
$ NODE_ENV=production (or 'SET NODE_ENV=production' for Windows)
$ npm install
$ npm run start

Open browser in: http://127.0.0.1:8100/
```


## License
----

MIT


[//]: # 
   [node.js]: <http://nodejs.org>
   [PDF]: <https://bitbucket.org/joelcorrales/react-test/raw/7c103810c7edd0c4debe0c4fdc6772c64afd7508/Azumo%20-%20Frontend%20Engineer%20(React)%20Challenge.pdf>