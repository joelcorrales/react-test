import React from "react";

export default class AppList extends React.Component {
	render() {
		var that = this;
		function addApp() {
			this.props.addApp(this.props.appName);
		}

		function removeApp() {
			this.props.removeApp(this.props.appName);
		}

		return (
			<li className={this.props.appClass} >
				{this.props.appName}
				<button className="btn btn-default plus" onClick={addApp.bind(this)}><i className="glyphicon glyphicon-plus"/></button>
				<button className="btn btn-default minus" onClick={removeApp.bind(this)}><i className="glyphicon glyphicon-minus"/></button>
			</li>
		);
	}
}