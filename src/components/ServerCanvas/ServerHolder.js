import React from "react";

export default class ServerHolder extends React.Component {
	render() {
		function serverAppList(instances) {
			var renderList = [];

			for (var i = 0; i < instances.length; i++) {
				
				renderList.push( <div key={i} className="col-md-5">{ instances[i].name }</div> );
			}

			return (
				<div className="show-grid">
					{renderList}
				</div>
			);
		}
		return (
			<div className="server-holder text-center">
				<b>{ this.props.serverName }</b>
				{ serverAppList( this.props.instances ) }
			</div>
		);
	}
}