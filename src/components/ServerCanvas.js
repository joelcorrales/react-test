import React from "react";

import ServerHolder from './ServerCanvas/ServerHolder'

export default class ServerCanvas extends React.Component {
	render() {
		function getServerList(list) {
			var renderList = [];

			for (var i in list) {
				renderList.push( <ServerHolder serverName={list[i].name} instances={list[i].instances} key={i} /> );
			}

			return (
				<ul>
					{renderList}
				</ul>
			);
		};

		return (
			<div>
				<h1>Server Canvas</h1>
				<div>
					{ getServerList( this.props.serverList ) }
				</div>
			</div>
		);
	}
}