import React from "react";

export default class Spinner extends React.Component {
	render() {
		return (
			<view>
			<div className="loading-bg">
			</div>
			<div className="loading text-center">
				<div>
					<img src="./css/spin.gif"/>
					<span>{ this.props.message? this.props.message : "Loading..." }</span>
				</div>
			</div>
			</view>
		);
	}
}