import React from "react";

import AppList from "./AppManager/AppList";

export default class AppManager extends React.Component {
	render() {
		function getAppList(list) {
			var renderList = [];

			for (var i in list) {
				renderList.push( <AppList appName={list[i].name} appClass={'pos'+(i)} key={i} addApp={this.props.addApp} removeApp={this.props.removeApp} /> );
			}

			return (
				<ul>
					{renderList}
				</ul>
			);
		};

		return (
			<div>
				<div className="text-center server-button-holder">
					<button className="btn add-server" onClick={this.props.addServer}><spam className="glyphicon glyphicon-plus"></spam> Add Server</button>
					<button className="btn destryo-server" onClick={this.props.removeServer}><spam className="glyphicon glyphicon-minus"></spam> Destroy</button>
				</div>
				<h4>Available Apps</h4>
				{ getAppList.bind(this)( this.props.appList ) }
			</div>
		);
	}
}