import React from "react";
import ReactDOM from "react-dom";
import _ from "lodash";

/*Import Components*/
import AppManager from "../components/AppManager";
import ServerCanvas from "../components/ServerCanvas";
import Spinner from "../components/Spinner";

class Layout extends React.Component {
	constructor() {
		super();

		this.state = {
			showSpinner: false,
			servers : [{
					name: "server0",
					instances: []
				},{
					name: "server1",
					instances: []
				},{
					name: "server2",
					instances: []
				},{
					name: "server3",
					instances: []
				}],
			apps : [{
					name: "Hadoop",
					instances: []
				},
				{
					name: "Rails",
					instances: []
				},
				{
					name: "Chronos",
					instances: []
				},
				{
					name: "Storm",
					instances: []
				},
				{
					name: "Spark",
					instances: []
				}]
		};
	}

	addServer() {
		this.showSpinner("Wait while the server is deployed...");
		setTimeout((function () {
					this.hideSpinner();
				}).bind(this), 3000);

		this.state.servers.push({
			name: "server" + this.state.servers.length,
			instances: []
		});

		this.updateState();
	}

	removeServer() {
		this.showSpinner("Wait while the server is removed...");
		setTimeout((function () {
					this.hideSpinner();
				}).bind(this), 3000);

		var server = _.last(this.state.servers);

		this.state.servers.pop();

		if (server.instances.length > 0) {
			this.realocateApps(server);
		}

		this.updateState();
	}

	realocateApps(server) {
		var that = this;
		_.forEach(server.instances, function realocateApp (app) {
			that.removeApp(app.name);
			that.addApp(app.name);
		})
	}

	updateState() {
		this.setState({
			servers : this.state.servers,
			apps : this.state.apps
		});
	}

	addApp (appId) {
		var app = _.find(this.state.apps, {name: appId});

		var server, count = 0;

		while ( _.isUndefined(server)  && count < 2 ) {
			server = _.find(this.state.servers, function (server) {
				return (server.instances.length === count);
			});

			count++;
		} 

		if ( _.isUndefined(server) ) {
			return false;
		} else {
			var newInstance = {
				name: appId,
				time: (new Date()).getTime()
			};

			server.instances.push(newInstance);
			app.instances.push(newInstance);
		}

		this.updateState();
		return true;
	}

	removeApp (appId) {
		var app = _.find(this.state.apps, {name: appId});

		var lastInstance = _.last(app.instances);

		app.instances.pop();

		if (!_.isUndefined(lastInstance)) {
			_.forEach(this.state.servers, function searchForLastInstance (elem) {
				_.remove(elem.instances, function removeInstance (instance) {
					return instance.time === lastInstance.time;
				});
			});
		}

		this.updateState();
	}

	showSpinner(displayMessage) {
		this.setState({
			showSpinner : true,
			displayMessage
		});
	}

	hideSpinner() {
		this.setState({
			showSpinner : false,
			displayMessage: null
		});
	}

	render() {
		return (
			<div className="row">
				<div className="col-xs-12 col-md-4 grey">
					<AppManager
						addApp={ this.addApp.bind(this) }
						removeApp={ this.removeApp.bind(this) }
						removeServer={ this.removeServer.bind(this) }
						addServer={ this.addServer.bind(this) }
						appList={ this.state.apps } />
				</div>
				<div className="col-xs-12 col-md-8 black">
					<ServerCanvas serverList={ this.state.servers } />
				</div>
				{ this.state.showSpinner ? <Spinner message={this.state.displayMessage} />: null }
			</div>
		);
	}
}

var app = document.getElementById("app");

ReactDOM.render(<Layout/>, app);